%%%-------------------------------------------------------------------
%%% @author Piotr
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 13. sty 2015 17:37
%%%-------------------------------------------------------------------
-module(main).
-author("Piotr").

-include_lib("stdlib/include/qlc.hrl").
-include("company.hrl").


%% API
-export([init/0, insert_emp/3, insert_single_employee/0, fill_up_db/0, raise/2, raise_for_Johnson/0, all_females/0, salary_above_two/0, all_females_qlc/0, select_employees/0, raise_females/1]).



%%%erl -mnesia dir '"/tmp/funky"'



init ()->
  mnesia:create_schema([node()]),
  mnesia:start(),

  mnesia:create_table(employee,
    [{attributes, record_info(fields, employee)}]),
  mnesia:create_table(dept,
    [{attributes, record_info(fields, dept)}]),
  mnesia:create_table(project,
    [{attributes, record_info(fields, project)}]),
  mnesia:create_table(manager, [{type, bag},
    {attributes, record_info(fields, manager)}]),
  mnesia:create_table(at_dep,
    [{attributes, record_info(fields, at_dep)}]),
  mnesia:create_table(in_proj, [{type, bag},
    {attributes, record_info(fields, in_proj)}]),
  mnesia:info().


insert_emp(Emp, DeptId, ProjNames) ->
  Ename = Emp#employee.name,
  Fun = fun() ->
    mnesia:write(Emp),
    AtDep = #at_dep{emp = Ename, dept_id = DeptId},
    mnesia:write(AtDep),
    mk_projs(Ename, ProjNames)
  end,
  mnesia:transaction(Fun).


mk_projs(Ename, [ProjName|Tail]) ->
  mnesia:write(#in_proj{emp = Ename, proj_name = ProjName}),
  mk_projs(Ename, Tail);
mk_projs(_, []) -> ok.

insert_single_employee() ->
  Emp  = #employee{emp_no= 104732,
    name = klacke,
    salary = 7,
    sex = male,
    phone = 98108,
    room_no = {221, 015}},
  insert_emp(Emp, 'B/SFR', [erlang, mnesia, otp]).

raise(Eno, Raise) ->
  F = fun() ->
    [E] = mnesia:read(employee, Eno, write),
    Salary = E#employee.salary + Raise,
    New = E#employee{salary = Salary},
    mnesia:write(New)
  end,
  mnesia:transaction(F).

all_females() ->
  F = fun() ->
    Female = #employee{sex = female, name = '$1', _ = '_'},
    mnesia:select(employee, [{Female, [], ['$1']}])
  end,
  mnesia:transaction(F).

all_females_qlc() ->
  F = fun() ->
    Q = qlc:q([E#employee.name || E <- mnesia:table(employee),
      E#employee.sex == female]),
    qlc:e(Q)
  end,
  mnesia:transaction(F).

salary_above_two() ->
  F = fun() ->
    Employee = #employee{name = '$1', salary = '$2', _='_'},
    Guard = {'>', '$2', 2},
    Result = '$1',
    mnesia:select(employee, [{Employee, [Guard], [Result]}])
  end,
  mnesia:transaction(F).

select_employees() ->
  F = fun() ->
    mnesia:select(employee, [{'_', [], ['$_']}])
  end,
  mnesia:transaction(F).


raise_females(Amount) ->
  F = fun() ->
    Q = qlc:q([E || E <- mnesia:table(employee),
      E#employee.sex == female]),
    Fs = qlc:e(Q),
    over_write(Fs, Amount)
  end,
  mnesia:transaction(F).

over_write([E|Tail], Amount) ->
  Salary = E#employee.salary + Amount,
  New = E#employee{salary = Salary},
  mnesia:write(New),
  1 + over_write(Tail, Amount);
over_write([], _) ->
  0.















fill_up_db() ->
  Emp1  = #employee{emp_no= 104465,
    name = "Johnson Torbjorn",
    salary = 1,
    sex = male,
    phone = 99184,
    room_no = {242, 038}},
  Emp2  = #employee{emp_no= 107912,
    name = "Carlsson Tuula",
    salary = 2,
    sex = female,
    phone = 94556,
    room_no = {242,056}},
  Emp3  = #employee{emp_no= 114872,
    name = "Dacker Bjarne",
    salary = 3,
    sex = male,
    phone = 99415,
    room_no = {221,035}},
  Emp4  = #employee{emp_no= 104531,
    name = "Nilsson Hans",
    salary = 3,
    sex = male,
    phone = 99495,
    room_no = {222,026}},
  Emp5  = #employee{emp_no= 104659,
    name = "Tornkvist Torbjorn",
    salary = 2,
    sex = male,
    phone = 99514,
    room_no = {222,022}},
  Emp6  = #employee{emp_no= 104733,
    name = "Wikstrom Claes",
    salary = 2,
    sex = male,
    phone = 999586,
    room_no = {221,015}},
  Emp7  = #employee{emp_no= 117716,
    name = "Fedoriw Anna",
    salary = 1,
    sex = female,
    phone = 99143,
    room_no = {221, 031}},
  Emp8  = #employee{emp_no= 115018,
    name = "Mattsson Hakan",
    salary = 3,
    sex = male,
    phone = 99251,
    room_no = {203, 348}},
  insert_emp(Emp1, 'B/SFR', [erlang]),
  insert_emp(Emp2, 'B/SF', [erlang, beam, otp]),
  insert_emp(Emp3, 'B/SFP', [erlang, mnesia, otp, beam]),
  insert_emp(Emp4, 'B/SFR', [wolf, mnesia]),
  insert_emp(Emp5, 'B/SFP', [erlang, mnesia, wolf, beam]),
  insert_emp(Emp6, 'B/SFP', [erlang, www, beam]),
  insert_emp(Emp7, 'B/SFR', [doucmentation, www, otp]),
  insert_emp(Emp8, 'B/SF', [www, wolf, documentation]).


raise_for_Johnson() ->
    raise(104465, 5).


